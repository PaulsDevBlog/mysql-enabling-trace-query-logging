MySQL: Script: Enabling Trace Query Logging for Debugging Purposes
====================

Great SQL debugging script.
 
This repo contains MySQL query trace logging enabling script discussed within the respective 
PaulsDevBlog.com article: https://www.paulsdevblog.com/mysql-enabling-trace-query-logging-for-debugging-purposes/ 

For other articles and code, please visit:
https://www.paulsdevblog.com
