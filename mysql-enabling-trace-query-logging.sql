-- SET global general_log = 1;
show variables like '%general_log%';
-- 1) Log into MySQL session with admin priv, execute: show variables like '%general_log%';
-- 2) See status of general_log and general_log_file
-- 3) Temporarily turn on query logging: SET global general_log = 1;
-- 4) At linux console: Tail log file: tail -f your_general_log_file_path
-- 5) When done monitoring, turn off query logging: SET global general_log = 0;
-- 6) If necessary, clear query log at linux console: cat /dev/null > your_general_log_file_path
